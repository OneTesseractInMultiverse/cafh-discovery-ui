import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HttpClientModule} from '@angular/common/http';
import {MatButtonModule, MatIconModule, MatListModule, MatSidenavModule, MatToolbarModule, MatTooltipModule} from '@angular/material';
import {TopNavbarComponent} from './navigation/top-navbar/top-navbar.component';
import {HomeComponent} from './pages/home/home.component';
import {InfoBoxComponent} from './pages/home/components/info-box/info-box.component';
import {SearchBoxComponent} from './pages/home/components/search-box/search-box.component';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatInput, MatInputModule} from '@angular/material/input';
import { ResultsComponent } from './pages/results/results.component';
import { SearchComponent } from './pages/results/components/search/search.component';
import { ResultItemComponent } from './pages/results/components/result-item/result-item.component';
import { VideoComponent } from './pages/video/video.component';
import { PodcastComponent } from './pages/podcast/podcast.component';
import { LoginComponent } from './pages/login/login.component';
import { LoginItemComponent } from './pages/login/components/login-item/login-item.component';
import {AngularFontAwesomeModule} from 'angular-font-awesome';
import {MatCardModule} from '@angular/material/card';
import { TextComponent } from './pages/text/text.component';
import { TextItemComponent } from './pages/text/components/text-item/text-item.component';


@NgModule({
  declarations: [
    AppComponent,
    TopNavbarComponent,
    HomeComponent,
    InfoBoxComponent,
    SearchBoxComponent,
    ResultsComponent,
    SearchComponent,
    ResultItemComponent,
    VideoComponent,
    PodcastComponent,
    LoginComponent,
    LoginItemComponent,
    TextComponent,
    TextItemComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MatToolbarModule,
    MatSidenavModule,
    MatListModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    FlexLayoutModule,
    MatTooltipModule,
    AngularFontAwesomeModule,
    MatCardModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
