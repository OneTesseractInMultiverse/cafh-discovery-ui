import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-text-item',
  templateUrl: './text-item.component.html',
  styleUrls: ['./text-item.component.scss']
})
export class TextItemComponent implements OnInit {

  @Input() title;
  @Input() image;
  @Input() text;
  constructor() { }

  ngOnInit() {
  }

}
