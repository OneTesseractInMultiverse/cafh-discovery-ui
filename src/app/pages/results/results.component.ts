import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-results',
  templateUrl: './results.component.html',
  styleUrls: ['./results.component.scss']
})
export class ResultsComponent implements OnInit {

  textOne: string = 'Lorem Ipsum is simply dummy text-visualization of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text-visualization ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised ...';

  constructor() { }

  ngOnInit() {
  }

}
