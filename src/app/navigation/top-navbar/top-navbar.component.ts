import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {MenuItem} from '../../interfaces/menu-item';

@Component({
  selector: 'app-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.scss']
})
export class TopNavbarComponent implements OnInit {

  @Input() title = '';
  @Output() public sidenavToggle = new EventEmitter();
  // Private Attributes of the Component
  menuItems: MenuItem[] = [
    {title: 'Home', route: '/home'},
    {title: 'About', route: '/about'}
  ];

  constructor() {
  }

  ngOnInit() {
  }

  public onToggleSidenav = () => {
    this.sidenavToggle.emit();
  }

}
